package tech.jborn.shopify.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tech.jborn.shopify.db.entity.Order;
import tech.jborn.shopify.db.entity.OrderItem;
import tech.jborn.shopify.db.entity.Product;
import tech.jborn.shopify.db.repository.OrderRepository;
import tech.jborn.shopify.db.repository.ProductRepository;
import tech.jborn.shopify.exception.ResourceNotFoundException;
import tech.jborn.shopify.request.OrderItemRequest;
import tech.jborn.shopify.request.OrderRequest;
import tech.jborn.shopify.response.OrderItemResponse;
import tech.jborn.shopify.response.OrderResponse;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.math.BigDecimal.ZERO;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Slf4j
@RestController
@RequiredArgsConstructor
public class OrderController {
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    @Transactional
    @PostMapping("/orders")
    public OrderResponse createOrder(@RequestBody @Valid OrderRequest request) {
        List<Long> productsIds = extractProductsIds(request);
        Map<Long, Product> products = findProducts(productsIds);

        Order order = new Order();
        for (OrderItemRequest item : request.getItems()) {
            Product product = Optional.ofNullable(products.get(item.getProductId()))
                                      .orElseThrow(() -> new ResourceNotFoundException("Could not find product with id: " + item.getProductId()));
            order.addItem(product.getName(), item.getAmount(), product.getPrice());
        }

        return convertToResponse(
                orderRepository.save(order)
        );
    }

    private OrderResponse convertToResponse(Order order) {
        OrderResponse response = new OrderResponse();
        response.setOrderId(order.getId());
        response.setItems(convertToResponse(order.getItems()));

        BigDecimal total = calculateTotal(order);
        response.setBeforeDiscountTotal(total);
        response.setDiscount(ZERO);
        response.setTotal(total);

        return response;
    }

    private BigDecimal calculateTotal(Order order) {
        return order.getItems()
                    .stream()
                    .map(orderItem -> orderItem.getAmount().multiply(orderItem.getPrice()))
                    .reduce(ZERO, BigDecimal::add);
    }

    private List<OrderItemResponse> convertToResponse(List<OrderItem> items) {
        return items.stream()
                    .map(this::convertToResponse)
                    .collect(toList());
    }

    private OrderItemResponse convertToResponse(OrderItem item) {
        OrderItemResponse itemResponse = new OrderItemResponse();
        itemResponse.setOrderItemId(item.getId());
        itemResponse.setProductName(item.getProductName());
        itemResponse.setAmount(item.getAmount());
        itemResponse.setPrice(item.getPrice());

        return itemResponse;
    }

    private Map<Long, Product> findProducts(List<Long> productsIds) {
        return productRepository.findByIdIn(productsIds)
                                .stream()
                                .collect(toMap(Product::getId, identity()));
    }

    private List<Long> extractProductsIds(OrderRequest request) {
        return request.getItems()
                      .stream()
                      .map(OrderItemRequest::getProductId)
                      .collect(toList());
    }
}
