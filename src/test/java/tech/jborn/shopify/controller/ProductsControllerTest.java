package tech.jborn.shopify.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import tech.jborn.shopify.AbstractTest;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.core.Is.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

class ProductsControllerTest extends AbstractTest {

    @Test
    void getProducts() {
        given()
            .mockMvc(mockMvc)
            .auth().with(httpBasic(SERVER_USER, SERVER_PASSWORD))
        .when()
            .get("/products")
        .then()
            .status(HttpStatus.OK)
        .body("size()", is(4));
    }
}