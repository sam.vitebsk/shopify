FROM fabric8/java-alpine-openjdk11-jdk as builder
# первый этап для извлечения зависимостей
# базовый образ с псевдонимом builder

# задаем рабочую директорию
WORKDIR app

# копируем артифакт в контейнер в рабочую диркторию
COPY target/*.jar app.jar

# запускаем jar файл с опцией -Djarmode=layertools для возможности многоуровневоц обработки
RUN java -Djarmode=layertools -jar app.jar extract

# второй этам для копирования зависимостей из предыдущего образа
# базовый образ
FROM fabric8/java-alpine-openjdk11-jdk

# устанавливаем curl
RUN apk --no-cache add curl

# задаем рабочую директорию
WORKDIR app

# копируем зовисимости, которые не содержат SNAPSHOT
COPY --from=builder app/dependencies/ ./

# копируем классы загрузчика JAR
COPY --from=builder app/spring-boot-loader/ ./

# копируем зовисимости, которые содержат SNAPSHOT
COPY --from=builder app/snapshot-dependencies/ ./

# копируем классы приложения и ресурсы
COPY --from=builder app/application/ ./

# запускаем
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
